# Symf Amqp



## Getting started

- `composer install`
- `yarn`
- `yarn dev`
    - or `yarn watch`
- `cp .env.example .env`
    - complete if necessary
- `docker compose  up -d`
- `symfony console doctrine:migrations:migrate`
- `symfony console messenger:consume async -vv`
    - `symfony console messenger:stop-workers` to stop service


# Test
- `cp .env.test .env.test.local`
    - complete if necessary
- `php bin/console --env=test doctrine:database:create`
- `php bin/console --env=test doctrine:schema:create`
- `composer test`