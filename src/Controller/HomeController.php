<?php

namespace App\Controller;

use App\Dto\SumAnswer;
use App\Form\SumAnswerFormType;
use App\Repository\SumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home', methods: ['get','post'])]
    public function index(Request $request, MessageBusInterface $bus, SumRepository $repository): Response
    {
        $session = $request->getSession();
        $messages = $session->get('messages');
        $session->remove('messages');

        $form = $this->createForm(
            SumAnswerFormType::class,
            (new SumAnswer()),
            [
                'method' => 'POST'
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bus->dispatch($form->getData());
            $request->getSession()->set('messages', ['Your values has been sent.']);
            return $this->redirectToRoute('home');
        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
            'messages' => $messages,
            'lastRecords' => $repository->findBy([], ['id' => 'desc'], 5)
        ]);
    }
}
