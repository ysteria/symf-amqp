<?php

namespace App\Entity;

use App\Repository\SumRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SumRepository::class)
 */
class Sum
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number_0;

    /**
     * @ORM\Column(type="integer")
     */
    private $number_1;

    /**
     * @ORM\Column(type="integer")
     */
    private $result;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber0(): ?int
    {
        return $this->number_0;
    }

    public function setNumber0(int $number_0): self
    {
        $this->number_0 = $number_0;

        return $this;
    }

    public function getNumber1(): ?int
    {
        return $this->number_1;
    }

    public function setNumber1(int $number_1): self
    {
        $this->number_1 = $number_1;

        return $this;
    }

    public function getResult(): ?int
    {
        return $this->result;
    }

    public function setResult(): self
    {
        $this->result = $this->getNumber0() + $this->getNumber1();

        return $this;
    }
}
