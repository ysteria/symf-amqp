<?php

namespace App\MessageHandler;

use App\Dto\SumAnswer;
use App\Entity\Sum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SumAnswerHandler implements MessageHandlerInterface
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function __invoke(SumAnswer $sumAnswer)
    {
        $sum = new Sum();
        $sum->setNumber0($sumAnswer->number_0);
        $sum->setNumber1($sumAnswer->number_1);
        $sum->setResult();
        $this->em->persist($sum);
        $this->em->flush();
    }
}
