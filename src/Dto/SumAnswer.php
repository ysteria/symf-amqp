<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class SumAnswer
{
    public int $number_0 = 0;
    public int $number_1 = 0;

    public function __construct(array $values = [])
    {
        if ($values) {
            foreach (['number_0', 'number_1'] as $key) {
                $this->{$key} = $values[$key] ?? 0;
            }
        }
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata->addPropertyConstraint('number_0', new Positive());
        $metadata->addPropertyConstraint('number_1', new Positive());
    }
}
