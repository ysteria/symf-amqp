<?php

namespace App\Tests\Web;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeTest extends WebTestCase
{
    public function testCanAccessHomepage(): void
    {
        $client = static::createClient();

        // Request a specific page
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Sum Answer Form');
        $this->assertFormValue('form', 'sum_answer_form[number_0]', '0');
        $this->assertFormValue('form', 'sum_answer_form[number_1]', '0');
    }
}
