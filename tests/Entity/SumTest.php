<?php

namespace App\Tests\Entity;

use App\Entity\Sum;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SumTest extends KernelTestCase
{
    public function testSumResultSetter()
    {
        $sumEntity = new Sum();
        $sumEntity->setNumber0(2);
        $sumEntity->setNumber1(5);
        $sumEntity->setResult();

        $this->assertEquals($sumEntity->getResult(), 7);
    }
}
